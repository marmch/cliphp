<?php
/**

  _____  _    _ _____     _____ _      _____ 
 |  __ \| |  | |  __ \   / ____| |    |_   _|
 | |__) | |__| | |__) | | |    | |      | |  
 |  ___/|  __  |  ___/  | |    | |      | |  
 | |    | |  | | |      | |____| |____ _| |_ 
 |_|    |_|  |_|_|       \_____|______|_____|
                                             
                                             

Command Line Interface Class
Copyright (C)2011-2016  David mArm Ansermot
Version 1.1.1
Repository https://bitbucket.org/marmch/cliphp

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

03-06-2016 - 1.1.1
- Added static outColor()

30-05-2016 - 1.1.0 
- Added color constants. 
- Added static outNLColor() methods for colored text

20-08-2011 - 1.0.0 
- First version

*/

/* Colors constants */
define('COLOR_BLACK', "\033[0;30m");
define('COLOR_BLUE', "\033[0;34m");
define('COLOR_GREEN', "\033[0;32m");
define('COLOR_CYAN', "\033[0;36m");
define('COLOR_RED', "\033[0;31m");
define('COLOR_PURPLE', "\033[0;35m");
define('COLOR_BROWN', "\033[0;33m");
define('COLOR_LIGHTGRAY', "\033[0;37m");
define('COLOR_DARKGRAY', "\033[0;90m");
define('COLOR_LIGHTBLUE', "\033[0;94m");
define('COLOR_LIGHTGREEN', "\033[0;92m");
define('COLOR_LIGHTCYAN', "\033[0;96m");
define('COLOR_LIGHTPURPLE', "\033[0;95m");
define('COLOR_YELLOW', "\033[0;93m");
define('COLOR_WHITE', "\033[0;97m");

define('COLOR_BLACK_B', "\033[1;30m");
define('COLOR_BLUE_B', "\033[1;34m");
define('COLOR_GREEN_B', "\033[1;32m");
define('COLOR_CYAN_B', "\033[1;36m");
define('COLOR_RED_B', "\033[1;31m");
define('COLOR_PURPLE_B', "\033[1;35m");
define('COLOR_BROWN_B', "\033[1;33m");
define('COLOR_LIGHTGRAY_B', "\033[1;37m");
define('COLOR_DARKGRAY_B', "\033[1;90m");
define('COLOR_LIGHTBLUE_B', "\033[1;94m");
define('COLOR_LIGHTGREEN_B', "\033[1;92m");
define('COLOR_LIGHTCYAN_B', "\033[1;96m");
define('COLOR_LIGHTPURPLE_B', "\033[1;95m");
define('COLOR_YELLOW_B', "\033[1;93m");
define('COLOR_WHITE_B', "\033[1;97m");

define('COLOR_CLOSE', "\033[0m");


/**
 * @author David Ansermot <dev@ansermot.ch>
 * @copyright (C)2011 ANSERMOT David, All rights reserved
 * @license GNU/GPL 2.0
 * @version 1.1
 * @date 2011-04-28
 * @filename class.cli.php
 */


/**
 * Console Layout Interface class
 */
class CLI {
	
	/**
	 * Ask user input
	 *
	 * @param string $text: The text to print
	 * @param bool $nl: New line at end ? (default: false)
	 * @return The user input
	 * @access public
	 * @static
	 */
	public static function ask($text, $nl = false) {
		
		if ($nl) { $text .= "\n"; }
			
		fwrite(STDOUT, $text);
		$resp = fgets(STDIN);
		
		return trim($resp);
	}
	
	
	
	
	
	/**
	 * Output content
	 *
	 * @param string $content: The content to print
	 * @return void
	 * @access public
	 * @static
	 */
	public static function out($content) {
		print($content);
	}
	

	/**
	 * Output content in color
	 *
	 * @param string $content: The content to print
	 * @param string $color: The color constant code to color message
	 * @return void
	 * @access public
	 * @static
	 * @since 1.1.1
	 */
	public static function outColor($content, $color) {
		print($content);
	}
	
	
	/**
	 * Output content width a new line at end
	 *
	 * @param string $content: The content to print
	 * @return void
	 * @access public
	 * @static
	 */
	public static function outNL($content) {
		print($content."\n");
	}
	

	/**
	 * Output content width a new line at end and a color
	 *
	 * @param string $content: The content to print
	 * @param string $color: The color constant code to color message
	 * @return void
	 * @access public
	 * @static
	 * @since 1.1.0
	 */
	public static function outNLColor($content, $color) {
		print($color.$content.COLOR_CLOSE."\n");
	}
	
	
	/**
	 * Output new line
	 *
	 * @param void
	 * @return void
	 * @access public
	 * @static
	 */
	public static function NL() {
		print("\n");
	}
	
	
	
	
	
	/**
	 * Parse command line arguments
	 *
	 * @param string $arguments: The arguments to parse
	 * @return array The arguments parsed
	 * @access public
	 * @static
	 */
	public static function parseScriptArgs($arguments = null) {
		
		// Check if we have usable arguments
		if (isset($arguments) && count($arguments) == 1) {
			return array();
		}
		
		// Remove script name (arg[0])
		array_shift($arguments);
		
		// Output buffer
		$parsed = array();
		
		// Parse each arg
		foreach ($arguments as $arg) {
			
			//
			// var="val"
			//
			if (strpos($arg, '=') !== false) {
				
				$splited = explode('=', $arg);
				$parsed[$splited[0]] = $splited[1];
				
			//
			// --command
			//
			} else if (substr($arg, 0, 2) == '--') {
				
				$parsed[$arg] = true;
			
			//
			// -avfRc
			//
			} else if (substr($arg, 0, 1) == '-' && substr($arg, 0, 2) != '--') {

				// remove start "-"
				$arg = substr($arg, 1, (strlen($arg) - 1));
				$parts = array();
				
				// more than 1 argument
				if (strlen($arg) > 1) {
					for ($i = 0; $i < strlen($arg); $i++) {
						$parsed[$arg{$i}] = true;	
					}
				} else {
					$parsed[$arg] = true;
				}
				
			}
			
		} // end foreach()
		
		return $parsed;
	}
	
}

?>